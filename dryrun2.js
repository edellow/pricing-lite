// Calculate Share portion but use calculator with the data i specfy
const {calcPrice,getSharePrice1,getAdminPrice1,getHiDPrice1} = require("./core-util/calculator")

// Build whatever set of factor data I want from files
// Only use this years data
const factors2021   = require("./data/factors2021")
const factors = [factors2021]


let parms = {
    coverageDate : '2021-09-09T04:00:00Z',
    productId    : 1,             // Share
    progLevelId  : 10,            //MS-1 10,11,12,13   CS-1 14,15  FL-1 16,17,18,19   
    age          : 50,
    tier         : 2,             // 1-single 2-couple 3-family
    marital      : 'Married',     // "Married" or "Unmarried"
    state        : "CO"
}

let v = {};  // Place to stash values for easy logging
share= getSharePrice1(v,parms,factors)    // Use a function in the calculator

console.log("Price=" + share.price)
console.log(share.v)