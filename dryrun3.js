// Calculate Share portion but use calculator with the data i specfy
// AND and a factor of my own after the fact from inline data

const {getFactor} = require("./core-util/FactorAccess")
const {getSharePrice1} = require("./core-util/calculator")

// Build whatever set of factor data I want from ANYWHERE
const factors2021   = require("./data/factors2021")
const factors2020   = require("./data/factors2020")
const bigFeetCostPlenty = 
{
coverageRange : { start: '2021-06-30T22:00:00Z' , end : '2099-06-30T22:00:00Z' },
name : "Ed's big foot factor",
factors : [
    {
        factorName: 'footsize',
        factorValues: [
        {            value : '8',    factor : 0.830        },
        {            value : '9',    factor : 1.030        },
        {            value : '10',   factor : 1.050        },
        {            value : '10.5', factor : 5.030        }   
        ]
    }
 ]
}
const factors = [factors2021,factors2020,bigFeetCostPlenty]

let parms = {
    coverageDate : '2021-09-09T04:00:00Z',
    productId    : 1,             // Share
    progLevelId  : 10,            //MS-1 10,11,12,13   CS-1 14,15  FL-1 16,17,18,19   
    age          : 50,
    tier         : 2,             // 1-single 2-couple 3-family
    marital      : 'Married',     // "Married" or "Unmarried"
    state        : "CO",
    footsize     : '10.5'
}

let v = {};  // Place to stash values for easy logging
v.footFactor    = getFactor(factors,parms.coverageDate,'footsize',parms.footsize)  

share= getSharePrice1(v,parms,factors)    // Use a function in the calculator for base share price
share.price = share.price*v.footFactor
console.log("Price=" + share.price)
console.log(share.v)