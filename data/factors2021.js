const factors2021 = 
{
coverageRange : { start: '2021-06-30T22:00:00Z' , end : '2099-06-30T22:00:00Z' },
name : "2021 Base pricing",
factors : [
    {
        factorName: 'marital',
        factorValues: [
        {            value : 'Unmarried', factor : 0.8300000000000000        },
        {            value : 'Married',   factor : 1.0300822880500500        }
        ]
    },{
        factorName: 'tier',
        factorValues: [
        {            value : 1,           factor : 0.5147806546225800        },
        {            value : 2,           factor : 0.9266051783206440        },
        {            value : 3,           factor : 1.4156468002121010        }
        ]
    },{
        factorName : 'geo',
        factorValues : 
        [
        {	value :	'MO'	, factor:	0.950		},
        {	value :	'NC'	, factor:	0.950		},
        {	value :	'OH'	, factor:	0.950		},
        {	value :	'PA'	, factor:	0.950		},
        {	value :	'GA'	, factor:	0.980		},
        {	value :	'CA'	, factor:	1.050		},
        {	value :	'CO'	, factor:	1.050		},
        {	value :	'FL'	, factor:	1.100		},
        {	value :	'TX'	, factor:	1.100		}
        ]
    },{
        factorName : 'age',
        factorValues : 
        [ 
        {           min  : 00,            max : 18,            factor : 0.43860907362578   },
        {           min  : 19,            max : 19,            factor : 0.4936707103100210 },
        {           min  : 20,            max : 20,            factor : 0.5500000000000000 },
        {           min  : 21,            max : 21,            factor : 0.6200000000000000 },
        {           min  : 22,            max : 22,            factor : 0.6807353841551650 },
        {           min  : 23,            max : 23,            factor : 0.7450000000000000 },
        {           min  : 24,            max : 24,            factor : 0.7900000000000000 },
        {           min  : 25,            max : 25,            factor : 0.8149370758747800 },
        {           min  : 26,            max : 26,            factor : 0.8350000000000000 },
        {           min  : 27,            max : 27,            factor : 0.8450000000000000 },
        {           min  : 28,            max : 28,            factor : 0.8508686115083370 },
        {           min  : 29,            max : 29,            factor : 0.8544174939589970 },
        {           min  : 30,            max : 30,            factor : 0.8579663764096570 },
        {           min  : 31,            max : 31,            factor : 0.8615152588603180 },
        {           min  : 32,            max : 32,            factor : 0.8650641413109780 },
        {           min  : 33,            max : 33,            factor : 0.8686130237616380 },
        {           min  : 34,            max : 34,            factor : 0.8721619062122981 },
        {           min  : 35,            max : 35,            factor : 0.8757107886629580 },
        {           min  : 36,            max : 36,            factor : 0.8792596711136180 },
        {           min  : 37,            max : 37,            factor : 0.8828085535642780 },
        {           min  : 38,            max : 38,            factor : 0.8863574360149380 },
        {           min  : 39,            max : 39,            factor : 0.8899063184655990 },
        {           min  : 40,            max : 40,            factor : 0.8934552009162590 },
        {           min  : 41,            max : 41,            factor : 0.9000000000000000 },
        {           min  : 42,            max : 42,            factor : 0.9070313693603800 },
        {           min  : 43,            max : 43,            factor : 0.9220335992338310 },
        {           min  : 44,            max : 44,            factor : 0.9397948598886060 },
        {           min  : 45,            max : 45,            factor : 0.9575561205433820 },
        {           min  : 46,            max : 46,            factor : 0.9782488514033150 },
        {           min  : 47,            max : 47,            factor : 0.9970447486010870 },
        {           min  : 48,            max : 48,            factor : 1.0158406457988600 },
        {           min  : 49,            max : 49,            factor : 1.0363609372349600 },
        {           min  : 50,            max : 50,            factor : 1.0594678200285500 },
        {           min  : 51,            max : 51,            factor : 1.0825747028221400 },
        {           min  : 52,            max : 52,            factor : 1.1055091461919000 },
        {           min  : 53,            max : 53,            factor : 1.1400000000000000 },
        {           min  : 54,            max : 54,            factor : 1.1900000000000000 },
        {           min  : 55,            max : 55,            factor : 1.2450000000000001 },
        {           min  : 56,            max : 56,            factor : 1.3000000000000000 },
        {           min  : 57,            max : 57,            factor : 1.3550000000000000 },
        {           min  : 58,            max : 58,            factor : 1.4200000000000000 },
        {           min  : 59,            max : 59,            factor : 1.4906732830306100 },
        {           min  : 60,            max : 60,            factor : 1.5768763346789800 },
        {           min  : 61,            max : 61,            factor : 1.6593067835552000 },
        {           min  : 62,            max : 62,            factor : 1.7362301809153900 },
        {           min  : 63,            max : 63,            factor : 1.8057946816735600 },
        {           min  : 64,            max : 99,            factor : 1.8660310444019400 }
        ]
    },{
        factorName : 'age-ahp',
        group : 10,
        groupname : "MS3-3K",
        factorValues : 
        [
        {   min :   0  , max :  47   , factor :     1.5342279835446700 },
        {   min :   48  , max : 48   , factor : 	1.5262114398409900 },
        {   min :   49  , max : 49   , factor : 	1.5160710499035800 },
        {   min :   50  , max : 50   , factor : 	1.5049681528002800 },
        {   min :   51  , max : 51   , factor : 	1.4941835185108600 },
        {   min :   52  , max : 52   , factor : 	1.4837786570066700 },
        {   min :   53  , max : 53   , factor : 	1.4686611812425000 },
        {   min :   54  , max : 54   , factor : 	1.4477966897833400 },
        {   min :   55  , max : 55   , factor : 	1.4261551765991500 },
        {   min :   56  , max : 56   , factor : 	1.4057522340090000 },
        {   min :   57  , max : 57   , factor : 	1.3864689076528400 },
        {   min :   58  , max : 58   , factor : 	1.3649826095973200 },
        {   min :   59  , max : 59   , factor : 	1.3430610175873800 },
        {   min :   60  , max : 60   , factor : 	1.3181272992698800 },
        {   min :   61  , max : 61   , factor : 	1.2959283884502500 },
        {   min :   62  , max : 62   , factor : 	1.2764999418662000 },
        {   min :   63  , max : 63   , factor : 	1.2598933295446800 },
        {   min :   64  , max : 64   , factor : 	1.2461881337704900 },
        ]
    },{
        factorName : 'age-ahp',                
        group : 11,
        name : "MS3-6K",
        factorValues : 
        [
        {   min :   00  , max : 47  , factor :	1.1119348279937800 },
        {   min :   48  , max : 48  , factor :	1.1099947763076100 },
        {   min :   49  , max : 49  , factor :	1.1075309847288300 },
        {   min :   50  , max : 50  , factor :	1.1048207019650200 },
        {   min :   51  , max : 51  , factor :	1.1021753165652800 },
        {   min :   52  , max : 52  , factor :	1.0996109942601400 },
        {   min :   53  , max : 53  , factor :	1.0958637776860400 },
        {   min :   54  , max : 54  , factor :	1.0906495552042800 },
        {   min :   55  , max : 55  , factor :	1.0851879458563600 },
        {   min :   56  , max : 56  , factor :	1.0799880769896500 },
        {   min :   57  , max : 57  , factor :	1.0750270983653700 },
        {   min :   58  , max : 58  , factor :	1.0694448815588900 },
        {   min :   59  , max : 59  , factor :	1.0636888694474900 },
        {   min :   60  , max : 60  , factor :	1.0570653042889400 },
        {   min :   61  , max : 61  , factor :	1.0510975763085400 },
        {   min :   62  , max : 62  , factor :	1.0458184516346600 },
        {   min :   63  , max : 63  , factor :	1.0412634534399200 },
        {   min :   64  , max : 99  , factor :	1.0374740372864500 }
        ]
    },{
        factorName : 'age-ahp',
        group : 12,
        groupName : "MS3-9K",
        factorValues : 
        [
        {   min :   00  , max : 00  , factor :	0.8574034554182020 },
        {   min :   48  , max : 48  , factor :	0.8562812425729650 },
        {   min :   49  , max : 49  , factor :	0.8548553680224820 },
        {   min :   50  , max : 50  , factor :	0.8532859248778670 },
        {   min :   51  , max : 51  , factor :	0.8517531333579120 },
        {   min :   52  , max : 52  , factor :	0.8502664333609740 },
        {   min :   53  , max : 53  , factor :	0.8480923747952540 },
        {   min :   54  , max : 54  , factor :	0.8450640939770060 },
        {   min :   55  , max : 55  , factor :	0.8418882536949750 },
        {   min :   56  , max : 56  , factor :	0.8388608961835950 },
        {   min :   57  , max : 57  , factor :	0.8359692218240080 },
        {   min :   58  , max : 58  , factor :	0.8327114455888060 },
        {   min :   59  , max : 59  , factor :	0.8293477875797690 },
        {   min :   60  , max : 60  , factor :	0.8254715182698690 },
        {   min :   61  , max : 61  , factor :	0.8219738577784290 },
        {   min :   62  , max : 62  , factor :	0.8188756446220170 },
        {   min :   63  , max : 63  , factor :	0.8161992636385700 },
        {   min :   64  , max : 64  , factor :	0.8139704848969360 }
        ]
    },{
        factorName : 'age-ahp',
        group : 13,
        name : "MS3-12K",
        factorValues : 
        [
        {   min :   00  , max : 00  , factor :	0.5703001429572700 },
        {   min :   62  , max : 62  , factor :	0.5760031443868430 },
        {   min :   63  , max : 63  , factor :	0.5817061458164160 },
        {   min :   64  , max : 64  , factor :	0.5874091472459890 }
    ]
    },{
        factorName : 'age-ahp-group',
        factorValues : 
        [
        {	value :	10  , factor:	10		}, // MS3-3K
        {	value :	11  , factor:	11		}, // MS3-6K
        {	value :	12	, factor:	12		}, // MS3-9K
        {	value :	13	, factor:	13		}, // MS3-12K

        {	value :	14	, factor:	10		}, // CS-1 3K
        {	value :	15	, factor:	11		}, // CS-1 6K

        {	value :	16	, factor:	10		}, // FK-1 3K
        {	value :	17	, factor:	11		}, // FL-1 6K
        {	value :	18	, factor:	12		}, // FL-1 9K
        {	value :	19	, factor:	13		}, // FL-1 12K
        ]
    },{
        factorName : 'age-ahp-discount',
        factorValues : 
        [
        {	value :	14	, factor:	0.850		}, // CS-1 3K
        {	value :	15	, factor:	0.850		}, // CS-1 6K

        {	value :	16	, factor:	0.700		}, // FK-1 3K
        {	value :	17	, factor:	0.700		}, // FL-1 6K
        {	value :	18	, factor:	0.700		}, // FL-1 9K
        {	value :	19	, factor:	0.700		}, // FL-1 12K
        ]
    },{
        factorName : 'base',
        factorValues : 
        [
        {	value :	1  , factor:	329.96 		},
        {	value :	2  , factor:	36.00		},
        {	value :	2  , factor:	30.00		},
        {   value : 8  , factor:    80          },
        {   value : 13 , factor:   	80          },
        {   value : 14 , factor:   	99          },
        {   value : 17 , factor:   	80          },
        {   value : 20 , factor:   	80          },
        {   value : 21 , factor:   	99          },
        {   value : 27 , factor:   	40          },
        {   value : 29 , factor:   	5           },
        {   value : 30 , factor:   	2           },
        {   value : 31 , factor:   	2           },
        {   value : 32 , factor:   	3           },
        {   value : 33 , factor:   	120         },
        {   value : 34 , factor:   	2           },
        {   value : 35 , factor:   	3           },
        {   value : 36 , factor:   	2           }
        ]
    },{
        factorName : 'product',
        factorValues : 
        [
        {	value :	'admin'	, factor:	0.083		},
        {	value :	'hid'	, factor:  -0.050		},
        ]
    }           
]
}
module.exports = factors2021;