// Get a factor for the available factor sets
const getFactor = function(factorSets,coverageDate,name,value,group= undefined,def=1)
{
    let i2=0;
    let _coverageDate = new Date(coverageDate);
    let returnVal=def;
    try
    {   
        const lname=name.toLowerCase()
        factorSets.find(factorSet => {
            if(factorSet.coverageRange === undefined || coverageDate === undefined || 
               ( _coverageDate >= new Date(factorSet.coverageRange.start) &&
                 _coverageDate <  new Date(factorSet.coverageRange.end)))
            {
                const i1=factorSet.factors.findIndex(function(factor,index){
                    return factor.factorName.toLowerCase() === lname &&
                            group === factor.group
                })
                if(i1>=0){
                    let factorValues=factorSet.factors[i1].factorValues
                    if(factorValues[0].value === undefined) {
                        i2=factorValues.findIndex(function(factor,index){
                            return ( value >= factor.min && value <= factor.max)
                        })
                    }
                    else {
                        i2=factorValues.findIndex(function(factor,index){
                            return factor.value === value
                        })
                    }
                    if(i2 >=0)
                    {
                        returnVal = factorValues[i2].factor;
                        return true;  // break
                    }
                }
            }
        });
    }
    catch
    {
        return def;
    }
    return returnVal;
}
module.exports = {getFactor}