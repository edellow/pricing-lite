const {calcPrice} = require("./core-util/calculator")

let parms = {
    coverageDate : '2021-09-09T04:00:00Z',
    productId    : 1,             // Share
    progLevelId  : 10,            //MS-1 10,11,12,13   CS-1 14,15  FL-1 16,17,18,19   
    age          : 50,
    tier         : 2,             // 1-single 2-couple 3-family
    marital      : 'Married',     // "Married" or "Unmarried"
    state        : "CO"
}
const  share = calcPrice(parms);

parms.productId = 2;  // Admin price
const  admin = calcPrice(parms);

parms.productId = 25;  // HiD price
const  hid = calcPrice(parms);

parms.productId = 33;  // App Fee price
const  AppFee = calcPrice(parms);

console.log([{"Share Portion" : share.price },{"Admin Portion" : admin.price},{"HID " : hid.price},{"App Fee " : AppFee.price}]);
console.log(hid.v)

