const {getFactor} = require("./FactorAccess")

// Build whatever set of data collection needed
const factors2021   = require("./../data/factors2021")
const factors2020   = require("./../data/factors2020")
const stdFactors = [factors2021,factors2020]

// Release dates of code used to decide which version
// of formulas to use
let releaseDate2020 = new Date('2020-08-31T18:00:00Z');
let releaseDate2021 = new Date('2021-06-30T22:00:00Z');

const calcPrice = function(parms)
{
    let v = {}  // Variable pool for factors
    const coverageDate = new Date(parms.coverageDate);
    if(coverageDate >= releaseDate2021 ) {
        v.calculator="2021"      
        switch(parms.productId)
        {
            case 1 :
                return getSharePrice1(v,parms,stdFactors);
            case 2 :
                return getAdminPrice1(v,parms,stdFactors);
            case 25 :
                return getHiDPrice1(v,parms,stdFactors);
        }
    }
    if(coverageDate >= releaseDate2020 && coverageDate < releaseDate2021 ) {
        v.calculator="2020"      
        switch(parms.productId)
        {
            case 1 :
                return getSharePrice1(v,parms,stdFactors);
            case 2 :
                return getAdminPrice1(v,parms,stdFactors);
            case 25 :
                return getHiDPrice1(v,parms,stdFactors);
        }
    }

    // Do something here for other prices and handle product not found
    let productPrice = getFactor(stdFactors,parms.coverageDate,'base',parms.productId, group=undefined,def=0)  // direct lookup
    return { "price" : productPrice, v:{}}
}

// Share Pricing Formula: 
// Base_Rate x Age_Factor x Tier_Factor x Marital_Factor x AHP-Age_Factor x Geo_Factor
const getSharePrice1 = function(v,parms,factors)
{
    v.base    = getFactor(factors,parms.coverageDate,'base',1)  // Share Product
    v.tier    = getFactor(factors,parms.coverageDate,'tier',parms.tier)
    v.marital = getFactor(factors,parms.coverageDate,'marital',parms.marital)
    v.age     = getFactor(factors,parms.coverageDate,'age',parms.age)
    v.geo     = getFactor(factors,parms.coverageDate,'geo',parms.state)

    // Age-AHP lookup using data that has been deduped using discount factors
    let=aaG  = getFactor(factors,parms.coverageDate,'age-ahp-group',parms.progLevelId)
    v.ageAhp = getFactor(factors,parms.coverageDate,'age-ahp',parms.age,aaG) * 
               getFactor(factors,parms.coverageDate,'age-ahp-discount',parms.progLevelId)

    let price=v.base * v.age  * v.tier * v.marital * v.ageAhp * v.geo 
    return( { price,v})
}

 //Admin Pricing Formula: Admin_Flat_Rate($36) + Share Portion x Admin%(8.34%)
 const getAdminPrice1 = function(v,parms,factors)
{
    let share = getSharePrice1 (v,parms,factors)
    v.adminBase    = getFactor(factors,parms.coverageDate,'base',2)           // Admin Portion
    v.adminPrecent = getFactor(factors,parms.coverageDate,'product','admin')  // Admin Percent
    
    let price = v.adminBase + share.price *  v.adminPrecent
    return( { price,v})
}

const getHiDPrice1 = function(v,parms,factors)
{
    let share = getSharePrice1 (v,parms,factors)
    v.hidPrecent   = getFactor(factors,parms.coverageDate,'product','hid')  // HiD Percent
    let price   = share.price * v.hidPrecent
    return( { price,v})
}

module.exports = {calcPrice,getSharePrice1,getAdminPrice1,getHiDPrice1}
