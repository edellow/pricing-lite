// Calculate Share portion just like the calculator would but in dry run code

const {getFactor} = require("./core-util/FactorAccess")

// Build whatever set of factor data I want from files
const factors2021   = require("./data/factors2021")
const factors2020   = require("./data/factors2020")
const factors = [factors2021,factors2020]


let parms = {
    coverageDate : '2021-06-09T04:00:00Z',
    productId    : 1,             // Share
    progLevelId  : 10,            //MS-1 10,11,12,13   CS-1 14,15  FL-1 16,17,18,19   
    age          : 50,
    tier         : 2,             // 1-single 2-couple 3-family
    marital      : 'Married',     // "Married" or "Unmarried"
    state        : "CO"
}

let v = {};  // Place to stash values for easy logging
v.base    = getFactor(factors,parms.coverageDate,'base',parms.productId)  // Share Product
v.tier    = getFactor(factors,parms.coverageDate,'tier',parms.tier)
v.marital = getFactor(factors,parms.coverageDate,'marital',parms.marital)
v.age     = getFactor(factors,parms.coverageDate,'age',parms.age)
v.geo     = getFactor(factors,parms.coverageDate,'geo',parms.state)

// Age-AHP lookup using data that has been deduped using discount factors
let=aaG  = getFactor(factors,parms.coverageDate,'age-ahp-group',parms.progLevelId)
v.ageAhp = getFactor(factors,parms.coverageDate,'age-ahp',parms.age,aaG) * 
           getFactor(factors,parms.coverageDate,'age-ahp-discount',parms.progLevelId)

let price=v.base * v.age  * v.tier * v.marital * v.ageAhp * v.geo 

console.log("Price=" + price)
console.log(v)